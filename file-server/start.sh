#!/bin/sh

if [ $# -lt 1 ] ; then
	echo "Usage: ./start.sh <PORT>"
	exit 1
fi

TEST_PORT=$1

docker run --name web -d -it -p $TEST_PORT:80 -v "$(pwd)"/files:/usr/share/nginx/html nginx
